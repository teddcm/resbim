<?php

$curl = curl_init();

$parameter = array(
    'key' => 'tDcG3VHP6qQgH2mw5fHvPw9InuTFesaw',
    'device' => '4bw0ds',
);
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://wapisender.com/api/v1/restart-device",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => 1,
    CURLOPT_POSTFIELDS => $parameter,
));

$response = curl_exec($curl);
curl_close($curl);
$json = json_decode($response, true);
return $json;
