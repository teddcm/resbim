<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    public function index()
    {
        $UserModel = new \App\Models\Auth\UserModel();
        $FAQModel = new \App\Models\Resbim\FAQModel();
        $QnAModel = new \App\Models\Resbim\QnAModel();
        $QnADetailModel = new \App\Models\Resbim\QnADetailModel();
        $data['pengguna'] = sizeof($UserModel->findAll());
        $data['permasalahan'] = sizeof($FAQModel->findAll());
        $data['konsultasi'] = sizeof($QnAModel->findAll());
        $data['percakapan'] = sizeof($QnADetailModel->findAll());
        $data['title'] = 'Dashboard';
        // d($data);

        return view('dashboard/index', $data);
    }
}
