<?php

namespace App\Models\Resbim;

use CodeIgniter\Model;

class QnAModel extends Model
{
    protected $table = 'resbim_qna';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $allowedFields = [
        'userid', 'detailcategoryid', 'status', 'name'
    ];

    protected $useTimestamps = true;

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    //--------------------------------------------------------------------
    // Category
    //--------------------------------------------------------------------
}
