<?php

namespace App\Models\Resbim;

use CodeIgniter\Model;

class FAQModel extends Model
{
    protected $table = 'resbim_faq';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $allowedFields = [
        'question', 'answer', 'order'
    ];

    protected $useTimestamps = true;

    protected $validationRules = [
        'question' => 'required',
        'answer' => 'required',
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;

    //--------------------------------------------------------------------
    // Category
    //--------------------------------------------------------------------
}
