<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>
                                <span class="row">
                                    <span class="col-lg-8">
                                        <small><?= $qna->category; ?> > <?= $qna->subcategory; ?></small><br />
                                        <span class="text-dark font-weight-bolder"><?= $qna->name; ?></span>
                                    </span>
                                    <span class="col-lg-4 text-right">
                                        <small><?= $qna->created_at; ?> </small><br />
                                    </span>
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="grupBuka">
                        <?php $i = 1; ?>
                        <?php foreach ($detail as $ctg) : ?>
                            <tr class="<?= $ctg->userid == $qna->userid ? 'text-left' : 'text-right' ?>">
                                <td style=" <?= $ctg->userid == $qna->userid ? 'padding-right:150px;' : 'padding-left:150px;' ?>" class="text-dark pb-3">
                                    <span class="p-2 card <?= $ctg->userid == $qna->userid ? 'text-left bg-success' : 'text-left bg-info' ?>">
                                        <span class="badge badge-light font-weight-bold"><?= ((empty($ctg->userfullname) ? $ctg->username :  $ctg->userfullname)); ?></span><br />
                                        <span class="text-light"><?= $ctg->content ?></span>

                                    </span>
                                    <br /><small style="font-size: 0.5rem;" class="text-dark pt-0 mt-0"><?= $ctg->updated_at; ?></small>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <form method="post" action="<?= base_url('resbim/qna_detail/add/' . $qna->crypt); ?>">
                <?php if ($qna->status != 3) : ?>
                    <input type="hidden" name="id" value="<?= $qna->crypt; ?>">
                    <input type="hidden" name="status" value="<?= user()->id == $qna->userid ? 1 : 2 ?>">
                    <?= csrf_field() ?>
                    <div class="form-group">
                        <label>Respon</label>
                        <textarea class="form-control  <?php if (session('errors.content')) : ?>is-invalid<?php endif ?>" name="content" id="content" rows="5"><?= old('content') ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Kirim</button>
                <?php endif; ?>
                <a class="btn btn-secondary float-left" href="<?= base_url('resbim/qna'); ?>">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>


<?= $this->section('js'); ?>
<?php if (in_groups('admin')) : ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/28.0.0/classic/ckeditor.js"></script>
<?php endif; ?>
<?= $this->endSection(); ?>