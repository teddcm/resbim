<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="card mb-3 p-2" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="<?= base_url('/img/' . $user->user_image); ?>" alt="<?= $user->fullname; ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <form class="user" method="post" action="<?= ('profile/' . $user->id); ?>">
                                <?= csrf_field() ?>
                                <input type="hidden" name="id" value="<?= @$user->id; ?>">
                                <input type="hidden" name="crypt" value="<?= @$user->crypt; ?>">

                                <div class="form-group pb-0">
                                    <input type="text" class="form-control" name="username" disabled value="<?= @$user->username; ?>" placeholder="Full Name">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="email" class="form-control text-success <?php if (session('errors.email')) : ?>is-invalid<?php endif ?>" name="email" value="<?= $user->email; ?>" placeholder="Isikan Email">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="text" class="form-control text-success <?php if (session('errors.fullname')) : ?>is-invalid<?php endif ?>" name="fullname" value="<?= @$user->fullname; ?>" placeholder="Isikan nama lengkap">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="number" class="form-control text-success <?php if (session('errors.whatsapp')) : ?>is-invalid<?php endif ?>" name="whatsapp" value="<?= $user->whatsapp; ?>" placeholder="Isikan nomor whatsapp">
                                    <small id="waHelp" class="form-text text-muted">diawali dengan 62, misal 6285640xxxxxx.</small>
                                </div>
                                <hr>
                                <p class="font-weight-bold">Ubah Kata Sandi</p>
                                <small id="waHelp" class="form-text text-muted">Silakan diisi jika ingin mengubah kata sandi</small>


                                <div class="form-group pb-0">
                                    <input type="password" name="password" class="form-control form-control <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?= lang('Auth.password') ?>" autocomplete="off">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="password" name="pass_confirm" class="form-control form-control <?php if (session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" placeholder="<?= lang('Auth.repeatPassword') ?>" autocomplete="off">
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>