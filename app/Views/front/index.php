<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title  -->
    <meta name="Author" content="Wiwie_kw">
    <title>Kantor Regional VIII BKN</title>
    <!-- Favicon  -->
    <link rel="icon" href="<?= base_url('resbim'); ?>/img/core-img/GARUDA.ico">
    <!-- Core Style CSS -->
    <link rel="stylesheet" href="<?= base_url('resbim'); ?>/asset/css/core-style.css">
    <link rel="stylesheet" href="<?= base_url('resbim'); ?>/asset/css/style.css">
    <!-- Responsive CSS -->
    <link href="<?= base_url('resbim'); ?>/asset/css/responsive.css" rel="stylesheet">
    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="<?= base_url('resbim'); ?>/asset/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?= base_url('resbim'); ?>/asset/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?= base_url('resbim'); ?>/asset/js/bootstrap.min.js"></script>
</head>

<body>
    <!-- Header Area Start -->
    <header class="header-area">
        <!-- Middle Header Area -->
        <div class="middle-header">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <!-- Logo Area -->
                    <div class="col-12 col-md-4">
                        <div class="logo-area">
                            <a href="https://banjarmasin.bkn.go.id/index.php"><img src="<?= base_url('resbim'); ?>/img/core-img/logo4.png" alt="logo"></a>
                        </div>
                    </div>
                    <!-- Header Advert Area -->
                    <div class="col-12 col-md-6" align="right">
                        <div class="header-advert-area">
                            <a href="https://banjarmasin.bkn.go.id/#"><img src="<?= base_url('resbim'); ?>/img/bg-img/top.png" alt="header-add"></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-2" align="left">
                        <div class="header-advert-area">
                            <div class="post-share-btn-group">
                                <a href="https://www.facebook.com/kanreg8bkn/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="https://twitter.com/kanreg8bkn" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/kanreg8bkn/?hl=id" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bottom Header Area -->
    </header>
    <!-- Header Area End -->
    <!---- menu-------------------------------------------------------------->
    <div class="bottom-header">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="main-menu">
                        <nav class="navbar navbar-expand-lg">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#gazetteMenu" aria-controls="gazetteMenu" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i> Menu</button>
                            <div class="collapse navbar-collapse" id="gazetteMenu">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="https://banjarmasin.bkn.go.id/index.php">Beranda <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="https://banjarmasin.bkn.go.id/#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Profil</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/./?set=viewProfil&id=33'>Sejarah</a> <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/./?set=viewProfil&id=37'>Visi dan Misi</a> <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/./?set=viewProfil&id=40'>Tugas dan Fungsi</a> <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/./?set=viewProfil&id=41'>Susunan Organisasi</a>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="https://banjarmasin.bkn.go.id/#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Berita</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/index.php?set=bk&id_kat=2&page=1&template3=1'>Berita dan Kegiatan</a>
                                            <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/index.php?set=bk&id_kat=4&page=1&template3=1'>Artikel</a>
                                            <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/index.php?set=pengumuman&jenis=1&page=1&template3=1'>Pengumuman</a>
                                            <a class='dropdown-item' href='https://banjarmasin.bkn.go.id/index.php?set=bk&id_kat=1&page=1&template3=1'>Press Release</a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="https://banjarmasin.bkn.go.id/index.php?set=video&page=1&template3=1">Video</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="https://banjarmasin.bkn.go.id/index.php?set=listDownload&jenis=1&page=1&template3=1">Download</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="https://banjarmasin.bkn.go.id/index.php?set=listRegulasi&jenis=1&page=1&template3=1">Regulasi</a>
                                    </li><li class="nav-item">
                                        <a class="nav-link" href="<?=base_url();?>">e-Resbim</a>
                                    </li>
                                    <!--<li class="nav-item">
                                 <a class="nav-link" href="#">sport</a>
                                 </li>-->
                                </ul>
                                <!-- Search Form 
                              <div class="header-search-form mr-auto">
                              <form action="#">
                                  <input type="search" placeholder="Input your keyword then press enter..." id="search" name="search">
                                  <input class="d-none" type="submit" value="submit">
                              </form>
                              </div>
                              <!-- Search btn 
                              <div id="searchbtn">
                              <i class="fa fa-search" aria-hidden="true"></i>
                              </div> -->
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Editorial Area Start -->
    <section class="catagory-welcome-post-area section_padding_50 ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gazette-post-tag">
                        <a href="https://banjarmasin.bkn.go.id/#">Data Permasalahan</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="dataTable" width="100%" cellspacing="0">
                            <tbody id="grupBuka">
                                <?php $i = 1; ?>
                                <?php foreach ($faq as $ctg) : ?>
                                    <tr>
                                        <td>
                                            <span class="strong" data-toggle="collapse" href="#buka<?= $ctg->id; ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                <b><u class="text-dark"><?= $ctg->question; ?> </u></b>
                                            </span>
                                            <div class="collapse pl-" id="buka<?= $ctg->id; ?>" data-parent="#grupBuka">
                                                <span class="text-primary"><?= $ctg->answer; ?></span>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section_padding_50"></section> 
    <section class="section_padding_50"></section> 
    <section class="section_padding_50"></section> 
    <!-- Footer Area Start -->
    <footer class="footer-area bg-img background-overlay" style="background-image: url(https://banjarmasin.bkn.go.id/img/bg-img//5.jpg);">
        <!-- Top Footer Area -->
        <!-- Bottom Footer Area -->
        <div class="bottom-footer-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center">
                    <div class="col-12">
                        <div class="copywrite-text">
                            <p>
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> Prakom Kanreg 8 <a href='https://banjarmasin.bkn.go.id/./?set=admin_login'><i class="fa fa-heart-o" aria-hidden="true"></i> </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->
    <!-- Plugins js -->
    <script src="<?= base_url('resbim'); ?>/asset/js/plugins.js"></script>
    <!-- Active js -->
    <script src="<?= base_url('resbim'); ?>/asset/js/active.js"></script>
</body>

</html>